<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	$list_users = json_decode(file_get_contents('login.json'), true);
	
	session_start();
	if (isset($_POST)){
	
		if (isset($_POST['user_name']) && (htmlspecialchars($_POST['user_name']) !== '')){
			unset($_SESSION['qname']);
			$user = htmlspecialchars($_POST['user_name']);
			if (array_key_exists($user, $list_users)) {
				if (isset($_POST['user_psw']) && (htmlspecialchars($_POST['user_psw']) === $list_users[$user]['password'])){
					$userpsw = $_POST['user_psw'];
					$_SESSION['auth'] = true;
			$_SESSION['qname'] = $userquest;
					$_SESSION['uname'] = $user;
					header('Location: list.php');
				} else {
					echo "Ошибка! Пароль неверный";
				}
			} else {
				echo "Ошибка! Такого пользователя нет или поле не заполнено";
			}
		} elseif (isset($_POST['user_quest']) && (htmlspecialchars($_POST['user_quest']))) {
			unset($_SESSION['uname']);
			$userquest = htmlspecialchars($_POST['user_quest']);
			$_SESSION['auth'] = false;
			$_SESSION['qname'] = $userquest;
			header('Location: list.php');
		}
	}
?>
	