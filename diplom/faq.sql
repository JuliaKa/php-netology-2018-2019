/* Запрос по созданию таблицы Темы */
CREATE TABLE `themes` (
	`id` int NOT NULL AUTO_INCREMENT, 
	`name` varchar(50) NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Запрос по созданию таблицы Вопросы и ответы */
CREATE TABLE `faq` (
	`id` int NOT NULL AUTO_INCREMENT, 
	`theme_id` int NOT NULL, 
	`question` varchar(50) NOT NULL, 
	`answer` varchar(50) NULL, 
	`status` varchar(20) NOT NULL DEFAULT 'hide', 
	`author` varchar(30) NULL, 
	`creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Запрос по созданию таблицы Администраторы */
CREATE TABLE `admins` (
	`id` int NOT NULL AUTO_INCREMENT, 
	`name` varchar(50) NOT NULL, 
	`password` varchar(30) NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Запрос по заполнениею таблицы Администраторы */
INSERT INTO `admins` (name, password)
	VALUES('admin', 'admin');
	
/* Запрос по заполнениею таблицы Темы */	
INSERT INTO `themes` (name)
	VALUES ('Blue theme');
	
/* Запрос по заполнениею таблицы Вопросы и ответы */
INSERT INTO `faq` (theme_id, question, answer, status, author, created_at)
	VALUES
		(1, 'Yes or no 2?', 'yes', 'published', 'Katya', NULL),
		(1, 'How are you 2?', NULL, NULL, 'Katya 1', '2018-10-12 03:03:03'),
		(4, 'Green or blue 2?', 'green', 'published', 'Admi', '2018-09-09 06:06:06');