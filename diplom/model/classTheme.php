<?php
    class Theme
    {
        public function getThemes()
        {
            $sqlThemeList = "SELECT id, name FROM `themes`";
            $stmtThemeList = db()->prepare($sqlThemeList);
            
            if ($stmtThemeList->execute()) {
                return $stmtThemeList->fetchAll();
            } else {
                die("Ошибка! <a href='index.php?page=admin'>Вернуться назад</a>");
            }
        }
        
        public function deleteTheme($id)
        {
            $sqlDelTheme = "DELETE FROM `themes` WHERE  `id` = '$id'";
            $sqlDelQuestions = "DELETE FROM `faq` WHERE  `theme_id` = '$id'";
            $listThemes = "SELECT 1 from `themes` WHERE `id` = '$id';";
            $masOfThemes = db()->prepare($listThemes);
            if ($masOfThemes->execute()) {
                $adminsMas = $masOfThemes->fetchAll();
                if (count($adminsMas) > 0) {
                    try {
                        db()->exec($sqlDelTheme);
                        db()->exec($sqlDelQuestions);
                        $res = "Изменения успешно сохранены";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось удалить тему<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Заданной темы не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function addTheme($newTheme)
        {
            $listThemes = "SELECT 1 from `themes` WHERE name='$newTheme';";
            $masOfThemes = db()->prepare($listThemes);
            if ($masOfThemes->execute()) {
                $adminsMas = $masOfThemes->fetchAll();
                $regexp_name = "/[A-Za-zА-Яа-я][A-Z0-9a-zА-Яа-я_]{2,}/i";
                if ((count($adminsMas) == 0) && preg_match($regexp_name, $newTheme)) {
                    $sqlCreateUser = "INSERT INTO `themes` (name) VALUES('$newTheme');";
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось создать тему $newTheme<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    $res = "К сожалению, тема с названием $newTheme уже существует или значение введено не верно. Пожалуйста, попробуйте снова.";
                }
            } else {
                die("Ошибка! " . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
            }
        }
        
        public function setThemeName($themeId, $themeName)
        {
            $listAnswers = "SELECT 1 from `themes` WHERE id='$themeId';";
            $masOfAnswers = db()->prepare($listAnswers);
            if ($masOfAnswers->execute()) {
                $mas = $masOfAnswers->fetchAll();
                if (count($mas) > 0) {
                    $sqlCreateUser = "UPDATE `themes` SET name='$themeName' WHERE id='$themeId';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось изменить ответ у вопроса<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
    }
