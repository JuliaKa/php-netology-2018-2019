<?php
    class Admin
    {
        public function getAdminsList()
        {
            $listUsers = "SELECT name, password FROM `admins`;";
            $masOfUsers = db()->prepare($listUsers);
            
            if ($masOfUsers->execute()) {
                return $masOfUsers->fetchAll();
            } else {
                die("Ошибка! <a href='index.php?page=admin'>Вернуться назад</a>");
            }
        }
        
        public function validateAdmin($user, $userpsw)
        {
            $listUsers = "SELECT name, password FROM `admins`;";
            $masOfUsers = db()->prepare($listUsers);
            
            if (isset($masOfUsers) && $masOfUsers->execute()) {
                $usersMas = $masOfUsers->fetchAll();
                $key = array_search($user, array_column($usersMas, 'name'));
                
                if (($key >= 0) && isset($userpsw) && (htmlspecialchars($userpsw) === $usersMas[$key]['password'])) {
                    $_SESSION['auth'] = true;
                    $_SESSION['uname'] = $user;
                    
                    header("Refresh: 0");
                } else {
                    $res = "Ошибка! Пароль или логин введены не верно.";
                    
                    include 'view/adminAutorizationPage.php';
                }
            } else {
                echo "Ошибка! Такого пользователя нет или поле не заполнено";
            }
        }
        
        public function deleteAdmin($columnName)
        {
            $sqlDel = "DELETE FROM `admins` WHERE  `name` = '$columnName'";
            $listAdmins = "SELECT 1 from `admins` WHERE name='$columnName';";
            $masOfAdmins = db()->prepare($listAdmins);
            if ($masOfAdmins->execute()) {
                $adminsMas = $masOfAdmins->fetchAll();
                if (count($adminsMas) > 0) {
                    try {
                        db()->exec($sqlDel);
                        $res = "Администратор $columnName успешно удален<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось удалить администратора $columnName<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Администратора с логином $columnName не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function addAdmin($uName, $uPsw)
        {
            $listAdmins = "SELECT 1 from `admins` WHERE name='$uName';";
            $masOfAdmins = db()->prepare($listAdmins);
            if ($masOfAdmins->execute()) {
                $adminsMas = $masOfAdmins->fetchAll();
                $regexp_name = "/[A-Za-z][A-Z0-9a-z_]{2,}/i";
                if ((count($adminsMas) == 0) && preg_match($regexp_name, $uName)) {
                    $sqlCreateUser = "INSERT INTO `admins` (name, password) VALUES('$uName', '$uPsw');";
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Администратор $uName успешно создан<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось создать администратора $uName<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    $res = "К сожалению, администратор с логином $uName уже существует или логин введено не верно. Введите, пожалуйста, логин, начиная с латинского символа.";
                }
            } else {
                die("Ошибка! " . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
            }
        }
        
        public function setAdminPassword($uName, $uPsw)
        {
            $listAdmins = "SELECT 1 from `admins` WHERE name='$uName';";
            $masOfAdmins = db()->prepare($listAdmins);
            if ($masOfAdmins->execute()) {
                $adminsMas = $masOfAdmins->fetchAll();
                if (count($adminsMas) > 0) {
                    $sqlCreateUser = "UPDATE `admins` SET password='$uPsw' WHERE name='$uName';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Пароль у администратора $uName успешно изменен<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось изменить пароль у администратора $uName<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Администратора с логином $uName не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
    }
