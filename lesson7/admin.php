<?php 
	include 'functions.php';
	if (!isset($_SESSION['uname'])){
		http_response_code(403);
		exit('<h1>403 Forbidden</h1><p>Перейти к <a href="index.php">форме авторизации</a></p>');
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.4 «Куки, сессии и авторизация»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
	<form action="admin.php" method="post" enctype="multipart/form-data">
		<p>Загрузить тест</p>
		<p><input name="uplfile" type="file"></p>
		<input type="submit" value="Загрузить">
	</form>
	<?php
		if ($_FILES){
			$uploaddir = 'tests/';
			$number = rand(1, 500);
			if ($_FILES['uplfile']['type'] === 'application/json'){
				if (move_uploaded_file($_FILES['uplfile']['tmp_name'], "$uploaddir/f$number.json")) {
					header('Location: list.php');
				} else {
					echo "Ошибка при перемещении";
				}
			} else {
				echo "Ошибка! Загрузите файл в формате .json";
			}
		}
	?>
	<p>Посмотреть список тестов можно по <a href="list.php">ссылке</a></p>
</body>
</html>