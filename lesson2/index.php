<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$arrayContinents = [
		"Africa" => [" Leopard", "Barchan cat", "Giraffe", "Lemuridae family", "Rhinoceros"],
		"America" => ["Red Lynx", "Wolverine ggg hhh ", "Jaguar", "Red wolf"],
		"Europe" => ["American mink", "Baikal seal", "Badger", "Wolf"],
		"Antarctica" => ["Penguin", "Albatross", "Leopard seal", "Blue whale"],
		"Asia" => ["Malay bear", "Saiga", "Asian lion", "Clouded leopard", "Argali"],
		"Australia" => ["Echidna", "Tasmanian devil", "Kangaroo", "Crowned pigeon", "Black Swan"]
	];

	foreach ($arrayContinents as $arrCt => $arrAnimals)
	{
		foreach ($arrAnimals as $animals){
			$posEnd = strripos(trim($animals), ' ');
			$posBegin = strpos(trim($animals), ' ');
			if (($posBegin != null) && ($posEnd != null) && ($posBegin == $posEnd)){
				$arrayWithAnimals[] =  $animals;
				$region[] = $arrCt;
			}
		}
	}
	foreach ($arrayWithAnimals as $animals){
		$arrayWithAnimalsWork[] =  explode(' ',$animals);
	}
		
	
	$arrayAnimalsCount = count($arrayWithAnimalsWork);
	
	for ( $i=0; $i < $arrayAnimalsCount; $i++ ){
		$arrayWithAnimalsBegin[] = $arrayWithAnimalsWork[$i][0];
		$arrayWithAnimalsEnd[] = $arrayWithAnimalsWork[$i][1];
	}
	
	shuffle($arrayWithAnimalsEnd);
	
	for ( $i=0; $i < $arrayAnimalsCount; $i++ ){
		$fantasticAnimals[] = $arrayWithAnimalsBegin[$i].' '. $arrayWithAnimalsEnd[$i];
	}
	
	$res = '';
	$regionAnimals[] = [];
	for ( $i=0; $i < $arrayAnimalsCount; $i++ ){
		if (($i == 0) || ($region[$i] == $region[$i-1])){
			$regionAnimals[$i] = $fantasticAnimals[$i];
		} else {
			$concatAnimal = implode(', ', $regionAnimals);
			$regionAnimals = null;
			$res .= "<h2>{$region[$i-1]}</h2><p>$concatAnimal</p>";
		}
	}
	
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Continents</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>

<body>
<h1>Основное задание</h1>
<h2>Исходный массив</h2>
<pre><?php print_r($arrayContinents);?></pre>
<h2>Звери, название которых состоит из двух слов</h2>
<pre><?php print_r($arrayWithAnimals);?></pre>
<h2>"Фантазийные" звери, название которых состоит из двух слов</h2>
<pre><?php print_r($fantasticAnimals);?></pre>
<h1>Дополнительно: </h1>
<?php echo $res; ?>
</body>
</html>