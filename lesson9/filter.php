<?php  
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$pdo = new PDO("mysql:host=localhost;dbname=global;charset=UTF8", "ykalashnikova", "neto1917");
 
	if(isset($_GET['name']) || isset($_GET['isbn']) || isset($_GET['author'])) {
		$sql = "SELECT * FROM books WHERE `isbn` LIKE '%" . $_GET['isbn'] . "%' AND `name` LIKE '%" . $_GET['name'] . "%' AND `author` LIKE '%" . $_GET['author'] . "%' ";	
	} else {
		$sql = "SELECT * FROM books";
	}
	
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$books = $stmt->fetchAll(PDO::FETCH_ASSOC); 
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Расширенное задание к лекции 4.1 «Реляционные базы данных и SQL»</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	</head>
	<body>
		
		<h1>Форма для фильтра таблицы</h1>
		
		<form name="form" action="filter.php" method="get">
			<input type="text" name="isbn" placeholder="ISBN" value="" />
			<input type="text" name="name" placeholder="Название книги" value="" />
			<input type="text" name="author" placeholder="Автор книги" value="" />
			<input value="Найти" type="submit" />
		</form>
		
		<table>
			<tr>
				<td>Идентификатор</td>
				<td>Название книги</td>
				<td>Автор</td>
				<td>Год</td>
				<td>isbn</td>
				<td>Жанр</td>
			</tr>
			<?php foreach ($books as $book):?>
					<tr>
						<td><?=$book['id']?></td>
						<td><?=$book['name']?></td>
						<td><?=$book['author']?></td>
						<td><?=$book['year']?></td>
						<td><?=$book['isbn']?></td>
						<td><?=$book['genre']?></td>
					</tr>
			<?php endforeach; ?>
		</table>
	</body>
</html>
