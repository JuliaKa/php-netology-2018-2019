<?php 
	class Car
	{
		public $marka;
		public $model;
		
		public function __construct($marka, $model)
		{
			$this->marka = $marka;
			$this->model = $model;
		}
		
		public function getBip()
		{
			echo 'Bip! Bip!<br>';
		}
	}
	
	$mazda = new Car('Mazda', '3');
	$mazda->getBip();
	$audi = new Car('Audi', 'Q6');
	
	
	class TV
	{
		public function __construct($marka, $model = '', $validateWorkTV = false)
		{
			$this->marka = $marka;
			$this->model = $model;
			$this->validateWorkTV = $validateWorkTV;
		}
		
		public function on()
		{	
			if ($this->validateWorkTV){
				echo 'TV set is already on<br>';
			} else {
				$this->validateWorkTV = true;
				echo 'On...<br>';
			}
		}
		
		public function off()
		{
			if ($this->validateWorkTV){
				echo 'Off...<br>';
				$this->validateWorkTV = false;
			} else {
				echo 'TV set is already off<br>';
			}
		}
		
		public function getPrice()
		{
			echo $price;
		}
	}
	
	$tvOne = new TV('LG');
	$tvOne->on();
	$tvOne->off();
	
	$tvTwo = new TV('Samsung', '2000');
	$tvTwo->off();
	
	
	class Pen
	{
		public $color;
		public $cost;
		
		public function __construct($color = 'blue')
		{
			$this->color = $color;
		}
		
		public function write()
		{
			echo 'Write...<br>';
		}
	}
	
	$penBlue = new Pen('blue');
	$penBlue->cost = 120;
	$penBlue->write();
	
	$penRed = new Pen('red');
	$penRed->cost = 20;
	echo $penRed->cost.'<br>';
	
	
	class Duck
	{
		protected $isFlying;
		
		public function __construct($name, $isFlying = true, $type = 'unknown')
		{
			$this->name = $name;
			$this->isFlying = $isFlying;
			$this->type = $type;
		}
		
		public function fly()
		{	
			if ($this->isFlying){
				echo 'Fly...<br>';
			} else {
				echo 'Can\'t fly...<br>';				
			}
		}
	}
	
	$duck = new Duck('Duckky');
	$duck->fly();
	
	$duckSmall = new Pen('Duck son', false);
	$duck->fly();
	
	
	class Product
	{
		public function __construct($name, $price, $discount = 0)
		{
			$this->name = $name;
			$this->price = $price;
			$this->discount = $discount;
		}
		
		public function getLowPrice(){
			echo ($this->price * (1 - $this->discount / 100)).'<br>';
		}
	}
	
	$car = new Product('Car', 1000000, 10);
	$car->getLowPrice();
	
	$milk = new Product('Milk', 60);
	$milk->getLowPrice();

?>