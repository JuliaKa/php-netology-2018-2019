<?php  
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	$host = 'localhost';
    $db   = 'ykalashnikova';
    $user = 'ykalashnikova';
    $pass = 'neto1917';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    
	try {
		$pdo = new PDO($dsn, $user, $pass, $opt);
	} catch (PDOException $e) {
		die('Подключение не удалось: ' . $e->getMessage());
	}
	
		
	//Запрос по созданию таблицы
	
	try {
		$sql = "CREATE TABLE `books_test_2` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(50) NULL, `author` varchar(50) NULL, `test` varchar(50) NULL, `genre` varchar(50) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";	
		if ($pdo->exec($sql)){
			echo "Таблица создана.";
		}
	} catch (PDOException $e) {
	}
	
	$sql = "SHOW TABLES";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$tables = $stmt->fetchAll();
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Задание к лекции 4.4 «Управление таблицами и базами данных»</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	</head>
	<body>
		
		<h1>Таблицы</h1>
		<ul>
		<?php foreach ($tables as $table):?>
			<?php foreach ($table as $value):?>
				<li><a href="index.php?table_name=<?= $value; ?>">Таблица <?= $value; ?></a></li>
			<?php endforeach; ?>
		<?php endforeach; ?>
		</ul>
		<?php if ((isset($_GET)) && (isset($_GET['table_name']))):?>
			<?php 
				$user_val_table = htmlspecialchars($_GET['table_name']);
				$sql = "DESCRIBE $user_val_table";
				$stmt = $pdo->prepare($sql);
				$stmt->execute(); 
				$rows = $stmt->fetchAll();
			?>
			<?php
				if (isset($_GET['del_col'])){
					$column_name = htmlspecialchars($_GET['del_col']);
					$sql_del = "ALTER TABLE $user_val_table DROP COLUMN $column_name";
					
					try {
						$pdo->exec($sql_del);
						echo "Удалено поле $column_name из таблицы $user_val_table<br />";
						echo '<a href="index.php">Обновить страницу</a>';
					} catch (PDOException $e) {
						echo "Из таблицы $user_val_table не удалось удалить поле $column_name<br />";
						die('Ошибка: ' . $e->getMessage());
					}
				}
				
				if (isset($_GET['set_name']) && isset($_GET['change_col'])){
					$regexp_name = "/[A-Za-z_][A-Z0-9a-z_]+/i";
					$old_name_column = htmlspecialchars($_GET['change_col']);
					$new_name_column = htmlspecialchars($_GET['set_name']);
					$select_data_column = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME=\"$user_val_table\" AND COLUMN_NAME=\"$old_name_column\"";
					try {
						$info_column = $pdo->prepare($select_data_column);
						$info_column->execute(); 
						$info = $info_column->fetchAll();
						/*echo "<pre>";
						print_r($info);
						echo "</pre>";*/
						$data_type = $info[0]['DATA_TYPE'];
						$char_max_length = ($info[0]['CHARACTER_MAXIMUM_LENGTH'])?'('.$info[0]['CHARACTER_MAXIMUM_LENGTH'].')': '';
						$is_nullable = ($info[0]['IS_NULLABLE'] == 'NO')? 'NOT NULL' : 'NULL';
					} catch (PDOException $e) {
						echo "Не удалось получить информацию о поле $old_name_column в таблице $user_val_table<br />";
						die('Ошибка: ' . $e->getMessage());
					}
					$change_name_column_sql = "ALTER TABLE $user_val_table CHANGE $old_name_column $new_name_column $data_type$char_max_length $is_nullable";
					
					if (preg_match($regexp_name, $new_name_column)){
						try {
							$pdo->exec($change_name_column_sql);
							echo "Измененилось название поля $old_name_column в таблицы $user_val_table. Переименовано в $new_name_column<br />";
							echo '<a href="index.php">Обновить страницу</a>';
						} catch (PDOException $e) {
							echo "Не удалось изменить поле $old_name_column в таблице $user_val_table<br />";
							die('Ошибка: ' . $e->getMessage());
						}
					} else {
						echo "Не удалось изменить поле $old_name_column в таблице $user_val_table<br />Введите название, используя латинские буквы,цифры или знак '_', начиная не с цифры";						
					}
				}
				
				if (isset($_GET['set_type']) && isset($_GET['change_col'])){
					$regexp_type = "/(int(\([1-9]+[0-9]*\)))|(varchar(\([1-9]+[0-9]*\)))|(char(\([1-9]+[0-9]*\)))|text/i";
					//$regexp_type = "/(int(\(1-90-9*\)))|(INT(\(1-90-9*\)))|(varchar(\(1-90-9*\)))|(VARCHAR(\(1-90-9*\)))|(char(\(1-90-9*\)))|(CHAR(\(1-90-9*\)))|text|TEXT/i";
					$name_column = htmlspecialchars($_GET['change_col']);
					$new_type_column = htmlspecialchars($_GET['set_type']);
					$select_data_column = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME=\"$user_val_table\" AND COLUMN_NAME=\"$name_column\"";
					try {
						$info_column = $pdo->prepare($select_data_column);
						$info_column->execute(); 
						$info = $info_column->fetchAll();
						/*echo "<pre>";
						print_r($info);
						echo "</pre>";*/
						$is_nullable = ($info[0]['IS_NULLABLE'] == 'NO')? 'NOT NULL' : 'NULL';
					} catch (PDOException $e) {
						echo "Не удалось получить информацию о поле $name_column в таблице $user_val_table<br />";
						die('Ошибка: ' . $e->getMessage());
					}
					
					if (preg_match($regexp_type, $new_type_column)){
						$change_type_column_sql = "ALTER TABLE $user_val_table MODIFY $name_column $new_type_column $is_nullable";
						try {
							$pdo->exec($change_type_column_sql);
							echo "Измененился тип поля $name_column в таблице $user_val_table. Переименовано в $new_type_column<br />";
							echo '<a href="index.php">Обновить страницу</a>';
						} catch (PDOException $e) {
							echo "Не удалось изменить поле $name_column в таблице $user_val_table. Запрос введен не верно<br />";
							echo $change_type_column_sql;
							die('Ошибка: ' . $e->getMessage());
						}
					} else {
						echo "Не удалось изменить поле $name_column в таблице $user_val_table<br />Введите корректный тип поля";						
					}
				}
			?>
			<table border="1">
				<?php foreach ( $rows as $value ):?>
					<tr>
						<td><?=$value['Field']?></td>
						<td>
							<form method="get" action="index.php">
								<input type="hidden" name="table_name" value="<?=$user_val_table?>">
								<input type="hidden" name="change_col" value="<?=$value['Field']?>">
								<input type="text" name="set_name" required>
								<input type="submit" value="Изменить название поля">
							</form>
						</td>
						<td><?=$value['Type']?></td>
						<td>
							<form method="get" action="index.php">
								<input type="hidden" name="table_name" value="<?=$user_val_table?>">
								<input type="hidden" name="change_col" value="<?=$value['Field']?>">
								<input type="text" name="set_type" required>
								<input type="submit" value="Изменить тип поля">
							</form>
						</td>
						<td><a href="index.php?table_name=<?=$user_val_table?>&del_col=<?=$value['Field']?>">Удалить поле</a></td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif;?>
	</body>
</html>
