<?php  
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$pdo = new PDO("mysql:host=localhost;dbname=global;charset=UTF8", "ykalashnikova", "neto1917");
	$sql = "SELECT * FROM books";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$books = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 4.1 «Реляционные базы данных и SQL»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
	<table>
		<tr>
			<td>Идентификатор</td>
			<td>Название книги</td>
			<td>Автор</td>
			<td>Год</td>
			<td>isbn</td>
			<td>Жанр</td>
		</tr>
		<?php foreach ($books as $book):?>
				<tr>
					<td><?=$book['id']?></td>
					<td><?=$book['name']?></td>
					<td><?=$book['author']?></td>
					<td><?=$book['year']?></td>
					<td><?=$book['isbn']?></td>
					<td><?=$book['genre']?></td>
				</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>