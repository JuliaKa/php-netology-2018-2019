<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    
    session_start();
    
    function db()
    {
        static $pdo = null;
        
        try {
            if ($pdo === null) {
                $config = [
                    'host' => 'localhost',
                    'db'   => 'ykalashnikova',
                    'user' => 'ykalashnikova',
                    'pass' => 'neto1917',
                    'charset' => 'utf8'
                ];

                $dsn = "mysql:host={$config['host']};dbname={$config['db']};charset={$config['charset']}";
                $opt = [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES   => false,
                ];
                $pdo = new PDO($dsn, $config['user'], $config['pass'], $opt);
            }
            return $pdo;
        } catch (PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }
    }
