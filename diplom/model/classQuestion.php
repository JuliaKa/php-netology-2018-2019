<?php
    class Question
    {
        public function getQuestionsListForUsers()
        {
            $sql = "SELECT themes.name AS theme, question, answer, author, created_at from `faq` JOIN themes ON themes.id = faq.theme_id WHERE status = 'published' ORDER BY theme;";
            $sqlCountList = "SELECT themes.id AS theme_id, themes.name AS theme, question, COUNT(*) AS count_theme from `faq` JOIN themes ON themes.id = faq.theme_id WHERE status = 'published' GROUP BY theme;";
            $stmt = db()->prepare($sql);
            $stmtList = db()->prepare($sqlCountList);
            
            if ($stmt->execute() && $stmtList->execute()) {
                $faq = $stmt->fetchAll();
                $faqListCount = $stmtList->fetchAll();
                $mas = ['faq' => $faq, 'faqListCount' => $faqListCount];
                return $mas;
            } else {
                die("Ошибка! <a href='index.php?page=admin'>Вернуться назад</a>");
            }
        }
     
        public function getQuestionsList()
        {
            $sql = "SELECT themes.id AS theme_id, themes.name AS theme, faq.id AS question_id, question, answer, status, author, created_at from `faq` JOIN themes ON themes.id = faq.theme_id  ORDER BY theme_id, created_at;";
            $sqlCountList = "SELECT themes.id AS theme_id, themes.name AS theme, COUNT(*) AS count_theme from `faq` JOIN `themes` ON themes.id = faq.theme_id GROUP BY theme_id;";
            $stmt = db()->prepare($sql);
            $stmtList = db()->prepare($sqlCountList);
            
            if ($stmt->execute() && $stmtList->execute()) {
                $faq = $stmt->fetchAll();
                $faqListCount = $stmtList->fetchAll();
                $mas = ['faq' => $faq, 'faqListCount' => $faqListCount];
                return $mas;
            } else {
                die("Ошибка! <a href='index.php?page=admin'>Вернуться назад</a>");
            }
        }
        
        public function deleteQuestion($questionId)
        {
            $sqlDel = "DELETE FROM `faq` WHERE `id` = '$questionId'";
            $listQuestions = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfQuestions = db()->prepare($listQuestions);
            if ($masOfQuestions->execute()) {
                $questionsMas = $masOfQuestions->fetchAll();
                if (count($questionsMas) > 0) {
                    try {
                        db()->exec($sqlDel);
                        $res = "Изменения успешно сохранены";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось удалить вопрос<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function addQuestion($newQuestion, $author, $selectedTheme)
        {
            $regexp_name = "/[A-Za-zА-Яа-я][A-Z0-9a-zА-Яа-я_]{2,}/i";
            if (preg_match($regexp_name, $newQuestion)) {
                $sqlCreateUser = "INSERT INTO `faq` (theme_id, question, status, author) VALUES ($selectedTheme,'$newQuestion','hide', '$author');";
                try {
                    db()->exec($sqlCreateUser);
                    $res = "Изменения успешно сохранены";
                } catch (PDOException $e) {
                    die("Ошибка! Не удалось создать вопрос '$newQuestion'<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                }
            } else {
                $res = "К сожалению, вопрос '$newQuestion' уже существует или значение введено не верно. Пожалуйста, попробуйте снова.";
            }
        }
        
        public function setQuestionName($questionId, $setQuestionValue)
        {
            $listQuestions = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfQuestions = db()->prepare($listQuestions);
            if ($masOfQuestions->execute()) {
                $mas = $masOfQuestions->fetchAll();
                if (count($mas) > 0) {
                    $sqlCreateUser = "UPDATE `faq` SET question='$setQuestionValue' WHERE id='$questionId';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось изменить вопрос<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function setQuestionTheme($questionId, $questionTheme)
        {
            $listQuestions = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfQuestions = db()->prepare($listQuestions);
            if ($masOfQuestions->execute()) {
                $questionsMas = $masOfQuestions->fetchAll();
                if (count($questionsMas) > 0) {
                    $sqlCreateUser = "UPDATE `faq` SET `theme_id` = '$questionTheme' WHERE id='$questionId';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось переместить вопрос в другую тему<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function setQuestionAnswer($questionId, $setAnswerValue)
        {
            $listAnswers = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfAnswers = db()->prepare($listAnswers);
            if ($masOfAnswers->execute()) {
                $mas = $masOfAnswers->fetchAll();
                if (count($mas) > 0) {
                    $sqlCreateUser = "UPDATE `faq` SET `answer` = '$setAnswerValue', `status` = 'processed' WHERE id='$questionId';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось изменить ответ у вопроса<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function setQuestionStatusOnProcessed($questionId)
        {
            $sqlSetStatus = "UPDATE `faq` SET status='processed' WHERE id='$questionId';";
            $listQuestions = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfQuestions = db()->prepare($listQuestions);
            if ($masOfQuestions->execute()) {
                $questionsMas = $masOfQuestions->fetchAll();
                if (count($questionsMas) > 0) {
                    try {
                        db()->exec($sqlSetStatus);
                        $res = "Изменения успешно сохранены";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось удалить вопрос<br />" . $e->getMessage() . '<a href="admin.php">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function setQuestionStatusOnPublish($questionId)
        {
            $sqlDel = "UPDATE `faq` SET status='published' WHERE id='$questionId';";
            $listAdmins = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfAdmins = db()->prepare($listAdmins);
            if ($masOfAdmins->execute()) {
                $adminsMas = $masOfAdmins->fetchAll();
                if (count($adminsMas) > 0) {
                    try {
                        db()->exec($sqlDel);
                        $res = "Изменения успешно сохранены";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось удалить вопрос<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
        
        public function setQuestionAuthor($questionId, $questionAuthor)
        {
            $listQuestions = "SELECT 1 from `faq` WHERE id='$questionId';";
            $masOfQuestions = db()->prepare($listQuestions);
            if ($masOfQuestions->execute()) {
                $questionsMas = $masOfQuestions->fetchAll();
                if (count($questionsMas) > 0) {
                    $sqlCreateUser = "UPDATE `faq` SET author='$questionAuthor' WHERE id='$questionId';";
                    
                    try {
                        db()->exec($sqlCreateUser);
                        $res = "Изменения успешно сохранены<br />";
                    } catch (PDOException $e) {
                        die("Ошибка! Не удалось изменить автора вопроса<br />" . $e->getMessage() . '<a href="index.php?page=admin">Вернуться назад</a>');
                    }
                } else {
                    die("Ошибка! Такого вопроса не существует. <a href='index.php?page=admin'>Вернуться назад</a>");
                }
            }
        }
    }
