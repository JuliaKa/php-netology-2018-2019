<?php

$query = urlencode($argv[1]);
if ($query) {
$result = json_decode(file_get_contents("https://www.googleapis.com/books/v1/volumes?q=$query"), true);
$file = fopen('books.csv', 'w');

if ($result !== FALSE){
	if (array_key_exists('items', $result)) {
		$items = $result['items'];
		foreach ($items as $field) {
			if ($field['volumeInfo']){
				$mas = [];
				$mas[0] = $field['volumeInfo']['title'];
				$mas[1] = implode(", ", $field['volumeInfo']['authors']);
				print_r($mas);
				fputcsv($file, $mas);
			}
		}
	} else {
		echo 'Книг нет.';
	}
	fclose($file);
} else {
	echo 'Ошибка! Файл с результатами поиска не получен.';
}
} else {
	echo 'Ошибка! Введите значение для поиска в строку.';
}

?>