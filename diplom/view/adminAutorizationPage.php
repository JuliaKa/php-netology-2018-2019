<!DOCTYPE HTML>
<html>
	<head>
		<title>Авторизация</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/reset.css"> 
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/design.css">
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
	</head>
	<body>
		<header>
			<h1>Вход в панель администратора</h1>
		</header>
		<section class="cd-faq">
			<ul class="cd-faq-categories">
				<li><a href="index.php">На главную</a></li>
			</ul>
			<div class="cd-faq-items">
				<h4 class="title-block">Доброго времени суток! Чтобы попасть в панель администратора, вам нужно авторизоваться</h4>
				<p style="text-align:center;"><?= $res; ?></p>
				<form class="form-block" action="index.php?page=admin" method="post">
					<input name="user_name" type="text" placeholder="Ваше имя"/>
					<input name="user_psw" type="text" placeholder="Ваш пароль"/>
					<input type="submit" value="Проверить">
				</form>
			</div>
		</section>
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/jquery.mobile.custom.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>