<?php
    include 'config.php';
    include 'model/classAdmin.php';
    include 'model/classTheme.php';
    include 'model/classQuestion.php';
    
    $adminsList = new Admin();
    $themesList = new Theme();
    $questionsList = new Question();
    $res = '';
    
    if (isset($_SESSION, $_SESSION['uname'])) {
        if (isset($_GET['logout'])) {
            session_destroy();
            header('Location: index.php');
        }
        
        if (isset($_GET['delete'])) {
            $columnName = htmlspecialchars($_GET['delete']);
            $adminsList->deleteAdmin($columnName);
        }
        
        if (isset($_POST['name'], $_POST['password'])) {
            $uName = htmlspecialchars($_POST['name']);
            $uPsw = htmlspecialchars($_POST['password']);
            $adminsList->addAdmin($uName, $uPsw);
        }
        
        if (isset($_POST['name'], $_POST['setPassword'])) {
            $uName = htmlspecialchars($_POST['name']);
            $uPsw = htmlspecialchars($_POST['setPassword']);
            $adminsList->setAdminPassword($uName, $uPsw);
        }
        
        if (isset($_GET['delete_theme'])) {
            $themeId = htmlspecialchars($_GET['delete_theme']);
            $themesList->deleteTheme($themeId);
        }
        
        if (isset($_POST['createTheme'])) {
            $newTheme = htmlspecialchars($_POST['createTheme']);
            $themesList->addTheme($newTheme);
        }
        
        if (isset($_POST['themeId'], $_POST['setTheme'])) {
            $themeId = htmlspecialchars($_POST['themeId']);
            $themeName = htmlspecialchars($_POST['setTheme']);
            $themesList->setThemeName($themeId, $themeName);
        }
        
        if (isset($_POST['qId'], $_POST['setAthor'])) {
            $questionId = htmlspecialchars($_POST['qId']);
            $questionAuthor = htmlspecialchars($_POST['setAthor']);
            $questionsList->setQuestionAuthor($questionId, $questionAuthor);
        }
        
        if (isset($_POST['qId'], $_POST['moveTheme'])) {
            $questionId = htmlspecialchars($_POST['qId']);
            $questionTheme = htmlspecialchars($_POST['moveTheme']);
            $questionsList->setQuestionTheme($questionId, $questionTheme);
        }
        
        if (isset($_POST['createQuestion'], $_POST['theme'])) {
            $newQuestion = htmlspecialchars($_POST['createQuestion']);
            $selectedTheme = (int)htmlspecialchars($_POST['theme']);
            $author = $_SESSION['uname'];
            $questionsList->addQuestion($newQuestion, $author, $selectedTheme);
        }
        
        if (isset($_POST['qId'], $_POST['setQuestion'])) {
            $questionId = htmlspecialchars($_POST['qId']);
            $setQuestionValue = htmlspecialchars($_POST['setQuestion']);
            $questionsList->setQuestionName($questionId, $setQuestionValue);
        }
        
        if (isset($_POST['qId'], $_POST['setAnswer'])) {
            $questionId = htmlspecialchars($_POST['qId']);
            $setAnswerValue = htmlspecialchars($_POST['setAnswer']);
            $questionsList->setQuestionAnswer($questionId, $setAnswerValue);
        }
        
        if (isset($_GET['delete_question'])) {
            $questionId = htmlspecialchars($_GET['delete_question']);
            $questionsList->deleteQuestion($questionId);
        }
        
        if (isset($_GET['process_question'])) {
            $questionId = htmlspecialchars($_GET['process_question']);
            $questionsList->setQuestionStatusOnProcessed($questionId);
        }
        
        if (isset($_GET['publish_question'])) {
            $questionId = htmlspecialchars($_GET['publish_question']);
            $questionsList->setQuestionStatusOnPublish($questionId);
        }
        
        $usersMas = $adminsList->getAdminsList();
        $qMas = $questionsList->getQuestionsList();
        $themeList = $themesList->getThemes();
        include 'view/adminPage.php';
    } elseif (isset($_POST, $_POST['user_name']) && (htmlspecialchars($_POST['user_name']) !== '')) {
        $adminsList->validateAdmin($_POST['user_name'], $_POST['user_psw']);
    } else {
        include 'view/adminAutorizationPage.php';
    }
