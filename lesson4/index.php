<?php
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	define('DATA_JSON_FILE',  'data.json');

	$res = [];
    if (!file_exists(DATA_JSON_FILE)) {
        echo 'Ошибка! Файл не найден!';
    } else {
		$json = file_get_contents(DATA_JSON_FILE);
		$arrayData = json_decode($json, true);
		if (!empty($arrayData)) {
			$res = $arrayData;
		}
	}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Телефонная книжка</title>
</head>
<body>
    <table border="1">
        <?php foreach ($res as $record): ?>
            <tr>
                <td><?= $record['firstName'] ?></td>
                <td><?= $record['lastName'] ?></td>
                <td><?= $record['address'] ?></td>
                <td><?= $record['phoneNumber'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>