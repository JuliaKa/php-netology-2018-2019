<?php
    
    if (isset($_GET, $_GET['page'])) {
        if ($_GET['page'] === 'admin') {
            include 'controller/adminController.php';
        }
        
        if ($_GET['page'] === 'main') {
            include 'controller/mainController.php';
        }
    } else {
        include 'controller/mainController.php';
    }
