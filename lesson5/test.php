<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	$res = '';
	$check_answers_value = '';
	if ($_GET && isset($_GET['test']) && ($_GET['test'] != '')){

		$uploaddir = '/var/www/user_data/ykalashnikova/lesson5/tests/';
		$tests_mas = scandir($uploaddir);
		$number = count($tests_mas) - 2;
		$test_list = [];
		if ($tests_mas && ($number > 0)){
			for ($i = 0; $i < $number; $i++){
				$test_list[$i] = $tests_mas[$i + 2];
			}
			if ($test_name = $test_list[(int)$_GET['test'] - 1]){
				$questions = json_decode(file_get_contents($uploaddir.$test_list[(int)$_GET['test'] - 1]), true);
				if ($_POST){
					$validate = false;
					foreach ($questions as $key => $question){
						if ($_POST[$key] == $question['answer']){
							$validate = true;
						} else {
							$validate = false;
						}
					}
					if ($validate){
						$check_answers_value = "Тест решен верно! Поздравляем! :)";
					} else {
						$check_answers_value = "Тест решен не верно. Попробуйте снова! :)";
					}
				}
			} else {
				$res = "Ошибка! Такого теста нет! Загрузка возможна по <a href='admin.php'>ссылке</a>";
			}
		} else {
			$res = "Ошибка! Тестов нет! Загрузка возможна по <a href='admin.php'>ссылке</a>";
		}
	} else {
		$res = "Ошибка! Номер теста не задан! Укажите параметр '?test='";
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.2 «Обработка форм»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<?php if (isset($test_list) && count($test_list) > 0): ?>
	<h1>Тест <?= $test_name; ?></h1>
	<form action="test.php?test=<?= $_GET['test'];?>" method="post">
		<?php foreach ($questions as $key => $question): ?>
			<fieldset>
				<legend><?= $question['question']; ?></legend>
				<?php foreach ($question['variants'] as $num => $variant): ?>
					<label><input type="radio" name="<?= $key; ?>" value="<?= $num; ?>" required><?= $variant; ?></label>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<input type="submit" value="Проверить">
	</form>
<?php endif; ?>
<p><?= $res; ?></p>
<p><?= $check_answers_value; ?></p>
<p>Список тестов можно посмотреть по <a href="list.php">ссылке</a></p>
</body>
</html>