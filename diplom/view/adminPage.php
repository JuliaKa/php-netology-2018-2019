<!DOCTYPE HTML>
<html>
	<head>
		<title>Диплом</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
	</head>
	<body>
		<main>
			<div class="container">
				<p><?="Привет, {$_SESSION['uname']}! (<a href='index.php?page=admin&logout=yes'>Выйти</a> | <a href='index.php'>Перейти на главную</a>)";?></p>
				
				<p><?= $res; ?></p>
				<h2>Редактируемые разделы:</h2>
				<h3>Темы, вопросы и ответы</h3>
				<?php $beginMass = 0; ?>		
				<?php foreach ($qMas['faqListCount'] as $theme):?>
					<?php $themeCount = (int)$theme['count_theme'];?>
					<ul>
						<li><?=$theme['theme']?> - <?=$theme['count_theme']?>
							<div>
								<form method="post" action="index.php?page=admin">
									<input type="hidden" name="themeId" value="<?= $theme['theme_id']; ?>">
									<input type="text" name="setTheme" required>
									<input type="submit" value="Переименовать">
								</form>
								<a href="index.php?page=admin&delete_theme=<?= $theme['theme_id']; ?>">Удалить тему</a>
							</div>
						</li>
					</ul>
					<?php $faq = $qMas['faq'];?>
					<table border="1">
						<?php $endMas = $beginMass + $themeCount;?>
						<?php for ($j=$beginMass; $j < $endMas; $j++):?>
							<tr data="<?= $faq[$j]['theme_id'];?> - <?= $faq[$j]['theme'];?>"> 
								<td>Вопрос - <?= $faq[$j]['question'];?></td>
								<td>Ответ - <?= $faq[$j]['answer'];?></td>
								<td>Информация ( дата создания: <?= $faq[$j]['created_at'];?>, автор: <?= $faq[$j]['author'];?>, статус: 
									<?php if ($faq[$j]['status'] == 'published'):?>
										Опубликовано
									<?php endif;?>
									<?php if ($faq[$j]['status'] == 'processed'):?>
										В обработке
									<?php endif;?>
									<?php if ($faq[$j]['status'] == 'hide'):?>
										Не опубликовано
									<?php endif;?> (
									<a href="index.php?page=admin&delete_question=<?= $faq[$j]['question_id'];?>">Удалить</a>
									<?php if ($faq[$j]['status'] == 'published'):?>
										 | <a href="index.php?page=admin&process_question=<?= $faq[$j]['question_id'];?>">Скрыть</a>
									<?php endif;?>
									<?php if ($faq[$j]['status'] == 'processed'):?>
										 | <a href="index.php?page=admin&publish_question=<?= $faq[$j]['question_id'];?>">Опубликовать</a>
									<?php endif;?>
									)
								</td>
								<td>
									<form method="post" action="index.php?page=admin">
										<input type="hidden" name="qId" value="<?= $faq[$j]['question_id'];?>">
										<input type="text" name="setAthor" required>
										<input type="submit" value="Изменить автора">
									</form>
								</td>
								<td>
									<form method="post" action="index.php?page=admin">
										<input type="hidden" name="qId" value="<?= $faq[$j]['question_id'];?>">
										<input type="text" name="setQuestion" required>
										<input type="submit" value="Изменить вопрос">
									</form>
								</td>
								<td>
									<form method="post" action="index.php?page=admin">
										<input type="hidden" name="qId" value="<?= $faq[$j]['question_id'];?>"> 
										<input type="text" name="setAnswer" required>
										<input type="submit" value="Изменить ответ">
									</form>
								</td>
								<td>
									<form method="post" action="index.php?page=admin">
										<input type="hidden" name="qId" value="<?= $faq[$j]['question_id'];?>">
										<select name="moveTheme">
											<option value="0" disabled selected>Переместить в другую тему</option>
											<?php foreach ($themeList as $key => $theme):?>
												<option value="<?=$theme['id']?>"><?=$theme['name']?></option>
											<?php endforeach; ?>
										</select>
										<input type="submit" value="Переместить">
									</form>
								</td>
							</tr>
						<?php endfor; ?>
						<?php $beginMass += $themeCount ;?>
					</table>
				<?php endforeach; ?>
				<h4>Создать новую тему</h4>
				<form method="post" action="index.php?page=admin">
					<input type="text" name="createTheme" required>
					<input type="submit" value="Создать">
				</form>
				<h4>Создать новый вопрос</h4>
				<form method="post" action="index.php?page=admin">
					<p><select name="theme">
						<option value="0" disabled selected>Выбрать тему вопроса</option>
						<?php foreach ($themeList as $key => $theme):?>
							<option value="<?=$theme['id']?>"><?=$theme['name']?></option>
						<?php endforeach; ?>
					</select></p>
					<p><textarea name="createQuestion" placeholder="Ваш вопрос" required ></textarea></p>
					
					<input type="submit" value="Создать">
				</form>
				<h3>Администраторы форума</h3>
				<table border="1">
					<tr>
						<th>№</th>
						<th>Логин</th>
						<th>Пароль</th>
						<th>Изменить пароль</th>
						<th>Удалить пользователя</th>
					</tr>
					<?php foreach ($usersMas as $key => $admin):?>
						<tr>
							<td><pre><?= ((int)$key +1) ?></pre></td>
							<td><?= $admin['name']; ?></td>
							<td><?= $admin['password']; ?></td>
							<td>
								<form method="post" action="index.php?page=admin">
									<input type="hidden" name="name" value="<?= $admin['name']; ?>">
									<input type="text" name="setPassword" required>
									<input type="submit" value="Изменить">
								</form>
							</td>
							<td><a href="index.php?page=admin&delete=<?=$admin['name']?>">Удалить</a></td>
						</tr>
					<?php endforeach; ?>
				</table>
				<h4>Создать нового администратора</h4>
				<form method="post" action="index.php?page=admin">
					<input type="text" name="name" required>
					<input type="text" name="password" required>
					<input type="submit" value="Создать">
				</form>
			</div>
		</main>
	</body>
</html>