<?php
    $beginMass = 0;
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Диплом</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
	</head>
	<body>
		<main>
			<div class="container">
				<p><?="Привет, {$_SESSION['uname']}! (<a href='?logout=yes'>Выйти</a> | <a href='?page=admin'>Перейти на панель администратора</a>)";?></p>
				<?php foreach ($qMas['faqListCount'] as $theme):?>
					<?php $themeCount = (int)$theme['count_theme'];?>
					<ul>
						<li><?=$theme['theme']?> - <?=$theme['count_theme']?></li>
					</ul>
					<?php $faq = $qMas['faq'];?>
					<table border="1">
						<?php $endMas = $beginMass + $themeCount;?>
						<?php for ($j=$beginMass; $j < $endMas; $j++):?>
							<tr>
								<?php foreach ($faq[$j] as $key => $value):?>
									<td><?=$value?></td>
								<?php endforeach; ?>
							</tr>
						<?php endfor; ?>
						<?php $beginMass += $themeCount;?>
					</table>
				<?php endforeach; ?>
			</div>
		</main>
	</body>
</html>