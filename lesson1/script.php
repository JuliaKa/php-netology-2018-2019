<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	if (!isset($_GET['x'])) {
		exit('Параметр х не был передан');
	} else {
		$x = $_GET['x'];
	}
	$param_a = 1;
	$param_b = 1;
	$res = '';
	
	while ($param_a < $x) {
		$param_c = $param_a;
		$param_a += $param_b;
		$param_b = $param_c;
	}
	
	if ($param_a > $x) {
		$res = '<p class="res">Задуманное число '.$x.' не входит в числовой ряд</p>';
	} elseif ($param_a == $x){
		$res = '<p class="res">Задуманное число '.$x.' входит в числовой ряд</p>';	
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Дополнительное задание к лекции 1.1 «Введение в PHP»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
	<h1>Дополнительное задание к лекции 1.1 «Введение в PHP»</h1>
	<h2>Вариант выполнения задания с передачей числа через адресную строку</h2>
	<p>Ввод числа пользователем осуществляется через адресную строку в виде <code>.../script.php?x=13</code>. По умолчанию задано число '<?= $x; ?>'.</p>
	
	<?= $res; ?>
</body>
</html>