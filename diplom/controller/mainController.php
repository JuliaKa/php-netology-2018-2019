<?php
    include 'config.php';
    include 'model/classQuestion.php';
    
    if (isset($_SESSION, $_SESSION['uname'], $_GET['logout'])) {
        session_destroy();
        header('Location: index.php');
    }
    
    try {
        $questionsList = new Question();
    
        $res = '';
        
        if (isset($_POST, $_POST['createQuestion'], $_POST['theme'], $_POST['author'])) {
            $newQuestion = htmlspecialchars($_POST['createQuestion']);
            $selectedTheme = (int)htmlspecialchars($_POST['theme']);
            $author = htmlspecialchars($_POST['author']);
            $questionsList->addQuestion($newQuestion, $author, $selectedTheme);
        }
                
        if (isset($_SESSION) && isset($_SESSION['uname'])) {
            
            /*$sql = "SELECT themes.name AS theme, question, answer, status, author, created_at from `faq` JOIN themes ON themes.id = faq.theme_id  ORDER BY theme;";
            $sqlCountList = "SELECT themes.name AS theme, question, COUNT(*) AS count_theme from `faq` JOIN themes ON themes.id = faq.theme_id GROUP BY theme;";
            $stmt = db()->prepare($sql);
            $stmtList = db()->prepare($sqlCountList);
            if ($stmt->execute() && $stmtList->execute()){
                $faq = $stmt->fetchAll();
                $faqListCount = $stmtList->fetchAll();
            }*/
            $qMas = $questionsList->getQuestionsList();
            include 'view/mainPageAdmin.php';
        } else {
            $qMas = $questionsList->getQuestionsListForUsers();
            include 'view/mainPageUser.php';
        }
    } catch (PDOException $e) {
        die('Ошибка при выводе таблицы: ' . $e->getMessage());
    }
