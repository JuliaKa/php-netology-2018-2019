<?php 
	$userName="Юлия";
	$userAge="23";
	$userEmail="yulechkaaa_1@mail.ru";
	$userLocation="Казань";
	$userAbout="Хочу научиться программировать на PHP"; 

?>
<!DOCTYPE HTML>
<html>
<head>
<title>About</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
        <h1>Страница пользователя: <?= $userName; ?></h1>
        <dl>
            <dt>Имя</dt>
            <dd><?= $userName; ?></dd>
        </dl>
        <dl>
            <dt>Возраст</dt>
            <dd><?= $userAge; ?></dd>
        </dl>
        <dl>
            <dt>Адрес электронной почты</dt>
            <dd><a href="mailto:<?= $userEmail; ?>"><?= $userEmail; ?></a></dd>
        </dl>
        <dl>
            <dt>Город</dt>
            <dd><?= $userLocation; ?></dd>
        </dl>
        <dl>
            <dt>О себе</dt>
            <dd><?= $userAbout; ?></dd>
        </dl>
    

</body>
</html>