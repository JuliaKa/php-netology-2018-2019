<?php

$sum = 0;
if ($argv[1] === '--today'){
	
	$row = 1;
	
	if (($filetxt = fopen('text.csv', 'r')) !== FALSE) {
		while (($data = fgetcsv($filetxt, 1000, ",")) !== FALSE) {
			$row++;
			if ($data[0] === date('Y-m-d')) {
				$sum += $data[1];
			}
		}
		fclose($filetxt);
		echo "Расход за день: ".$sum;
	}
	
} elseif ($argc < 3) {
	
    die(PHP_EOL . 'Ошибка! Аргументы не заданы. Укажите флаг --today или запустите скрипт с аргументами {цена} и {описание покупки}' . PHP_EOL);
	
} else {
	
	$filetxt1 = fopen('text.csv', 'a');
	$row_for_file[] = [];
	
	$row_for_file[0] = date('Y-m-d');
	$row_for_file[1] = (int)$argv[1];
	$row_for_file[2] =  implode(' ', array_slice($argv, 2));
	fputcsv($filetxt1, $row_for_file);

	if ($filetxt1 != FALSE) {
		fclose($filetxt1);
	}
	
	echo "Добавлена строка ".implode(', ', $row_for_file);
	
}
?>