<?php 
	include 'functions.php';
	if (!isset($_SESSION['uname']) && !isset($_SESSION['qname'])){
		http_response_code(403);
		exit('<h1>403 Forbidden</h1><p>Перейти к <a href="index.php">форме авторизации</a></p>');
	}

	$uploaddir = 'tests/';
	$tests_mas = scandir($uploaddir);
	$number = count($tests_mas) - 1;
	
	if(isset($_GET['delete_test']) && isset($_SESSION['uname']) && $_SESSION['auth']){
		$del_num = (int)$_GET['delete_test'];
		unlink("tests/{$tests_mas[$del_num]}");
		exit('<h1>Файл удален</h1><p>Перейти к <a href="list.php">списку тестов</a></p>');
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.3 «PHP и HTTP»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>Список тестов</h1>
<ol>
<?php if ($tests_mas && ($number > 1)): ?>
	<?php for ($i = 2; $i <= $number; $i++): ?>
		<li>
			<a href="test.php?test=<?= $i - 1 ?>"><?= $tests_mas[$i]; ?></a>
			<?php if (isset($_SESSION['uname']) && $_SESSION['auth']): ?>
				<a href="list.php?delete_test=<?= $i ?>">(Удалить тест)</a>
			<?php endif; ?>
		</li>
	<?php endfor; ?>
<?php endif; ?>
<?php if (isset($_SESSION['uname']) && $_SESSION['auth']): ?>
	<p>Загрузка возможна по <a href="admin.php">ссылке</a></p>
<?php endif; ?>
</ol>
</body>
</html>