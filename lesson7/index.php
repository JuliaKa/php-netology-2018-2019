<?php 
	include 'functions.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.4 «Куки, сессии и авторизация»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
	<h1>Доброго времени суток! Чтобы пройти тест, вам нужно авторизоваться или войти как гость</h1>
	<h2>Авторизоваться</h2>	
	<form action="index.php" method="post">
		<input name="user_name" type="text" placeholder="Ваше имя"/>
		<input name="user_psw" type="text" placeholder="Ваш пароль"/>
		<input type="submit" value="Проверить">
	</form>
	
	<h2>Войти, как гость</h2>	
	<form action="index.php" method="post">
		<input name="user_quest" type="text" placeholder="Ваше имя"/>
		<input type="submit" value="Проверить">
	</form>
</body>
</html>