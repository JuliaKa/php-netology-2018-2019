<?php
    $beginMass = 0;
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Диплом</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/reset.css"> 
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/design.css">
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
	</head>
	<body>
		<header>
			<h1>FAQ</h1>
		</header>
		<section class="cd-faq">
				<ul class="cd-faq-categories">
					<li><a href="?page=admin">Войти</a></li>
				</ul>
				<?= $res; ?>
				<div class="cd-faq-items">
					<?php foreach ($qMas['faqListCount'] as $theme):?>
						<ul class="cd-faq-group">
							<?php $themeCount = (int)$theme['count_theme'];?>
								<li class="cd-faq-title"><h2><?=$theme['theme']?> (<?=$theme['count_theme']?>)</h2></li>
							<?php $faq = $qMas['faq'];?>
								<?php $endMas = $beginMass + $themeCount;?>
								<?php for ($j=$beginMass; $j < $endMas; $j++):?>
									<li>
										<a class="cd-faq-trigger" href="#0"><?= $faq[$j]['question']; ?></a>
										<div class="cd-faq-content">
											<p><?= $faq[$j]['answer']; ?></p>
										</div>
									</li>
								<?php endfor; ?>
								<?php $beginMass += $themeCount; ?>
						</ul>
					<?php endforeach; ?>
				<h4 class="title-block">Задать вопрос</h4>
				
				<form class="form-block" method="post" action="index.php">
					<p><input name="author" placeholder="Ваше имя" type="text" required /></p>
					<p><input name="email" placeholder="Ваша эл. почта" type="text" required /></p>
					<p><select name="theme">
						<option value="0" disabled selected>Выбрать тему вопроса</option>
						<?php foreach ($qMas['faqListCount'] as $key => $theme):?>
							<option value="<?=$theme['theme_id']?>"><?=$theme['theme']?></option>
						<?php endforeach; ?>
					</select></p>
					<p><textarea name="createQuestion" placeholder="Ваш вопрос" required ></textarea></p>
					<input type="submit" value="Отправить">
				</form>
				</div>
		</section>
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/jquery.mobile.custom.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>