<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.2 «Обработка форм»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
	<form action="admin.php" method="post" enctype="multipart/form-data">
		<p>Загрузить тест</p>
		<p><input name="uplfile" type="file"></p>
		<input type="submit" value="Загрузить">
	</form>
	<?php
		if ($_FILES){
			$uploaddir = '/var/www/user_data/ykalashnikova/lesson5/tests/';
			$number = count(scandir($uploaddir)) - 1;
			if ($_FILES['uplfile']['type'] === 'application/json'){
				if (move_uploaded_file($_FILES['uplfile']['tmp_name'], "$uploaddir/$number.json")) {
					echo "Тест загружен.";
				} else {
					echo "Ошибка при перемещении";
				}
			} else {
				echo "Ошибка! Загрузите файл в формате .json";
			}
			//print_r($_FILES);
			//echo "\n".$number;
		}
	?>
	<p>Посмотреть список тестов можно по <a href="list.php">ссылке</a></p>
</body>
</html>