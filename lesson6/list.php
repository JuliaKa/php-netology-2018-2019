<?php 
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$uploaddir = '/var/www/user_data/ykalashnikova/lesson6/tests/';
	$tests_mas = scandir($uploaddir);
	$number = count($tests_mas) - 1;
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Задание к лекции 2.3 «PHP и HTTP»</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>Список тестов</h1>
<ol>
<?php if ($tests_mas && (($number - 1) > 0)): ?>
	<?php for ($i = 2; $i <= $number; $i++): ?>
		<li><?= $tests_mas[$i]; ?></li>
	<?php endfor; ?>
<?php endif; ?>
</ol>
<p>Загрузка возможна по <a href="admin.php">ссылке</a></p>
</body>
</html>