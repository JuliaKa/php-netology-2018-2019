<?php

$needed_country = $argv[1];
$res = '';
$row = 1;
$i= false;

if (($filetxt = fopen("https://raw.githubusercontent.com/netology-code/php-2-homeworks/master/files/countries/opendata.csv", 'r')) !== FALSE) {
	while ((($data = fgetcsv($filetxt, 2000, ",")) !== FALSE) && ($i == false)) {
		$row++;
		$mas = explode(",", implode(",", $data));
		if ($mas[1] === $needed_country) {
			$res = $mas[1].': '.$mas[4];
			$i = true;
		}
	}
	fclose($filetxt);

	if($res == ''){
		echo 'Ошибка! Введите страну верно.';
	} else {
		echo $res;
	}
} else {
	echo 'Файл не доступен.';
}

?>